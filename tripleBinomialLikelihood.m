function l = tripleBinomialLikelihood(x, n, p)
% x - vector of success counts
% n - vector of trial count
% p - vector of probabilities

l = x(1) * log(p(1)) + (n(1)-x(1)) * log(1-p(1)) + ...
    x(2) * log(p(2)) + (n(2)-x(2)) * log(1-p(2)) + ...
    x(3) * log(p(3)) + (n(3)-x(3)) * log(1-p(3));


