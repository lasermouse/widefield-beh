function hazardRate = earlyLickHazard(trials, bins)

if nargin<2
    bins = [0:16];
end

earlyLickCounts = histcounts([trials.lkFA], bins);
changeCounts = histcounts([trials(isnan([trials.lkFA])).stimT], bins);

totalTrials = numel(trials);

earlyLickCumsum = [ 0 cumsum(earlyLickCounts(1:end-1)) ];
changeCumsum = [ 0 cumsum(changeCounts(1:end-1)) ];

hazardRate = earlyLickCounts ./ ...
    (totalTrials - earlyLickCumsum - changeCumsum);