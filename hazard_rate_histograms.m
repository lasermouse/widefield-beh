mouse = 'IO_078';
thisMouse = allSesOut(strcmp({allSesOut.mouse},mouse));
% for ind = 1:numel(SessionData)
%     SessionData(ind).session = SessionData(ind).session{1};
% end
% 
% sessions = unique({SessionData([SessionData.RwdOmmited]==1).session});
%%
% thisMouse = SessionData(...%strcmp({SessionData.mouse},mouse) & ...
%     strcmp({SessionData.controlRunning},'running') & ...
%     strcmp({SessionData.hazard},'nonsplit') &...
%     ismember({SessionData.session}, sessions));

% find rewarded trial
rewards = strcmp({thisMouse.outcome},'Hit') & ...
    [thisMouse.change]>1 & [thisMouse.RwdOmmited]==0;

% find time of the last reward received 
lastReward = zeros(size(thisMouse));
for ind = 2:numel(lastReward)
    if rewards(ind)
        lastReward(ind) = thisMouse(ind).stimT;
    else
        lastReward(ind) = lastReward(ind-1);
    end
end

% find reward time on the previous trial
delta = 1;
previousTrialReward = nan(size(thisMouse));
previousTrialReward(find(rewards)+delta) = [thisMouse(rewards).stimT];
previousTrialReward = previousTrialReward(1:numel(thisMouse));

earlylicks = strcmp({thisMouse.outcome},'FA');
previousTrialLick = nan(size(thisMouse));
previousTrialLick(find(earlylicks)+delta) = [thisMouse(earlylicks).lkFA];
previousTrialLick = previousTrialLick(1:numel(thisMouse));

omittedrewards = strcmp({thisMouse.outcome},'Hit') & ...
    [thisMouse.change]>1 & [thisMouse.RwdOmmited]==1;
previousTrialOmitted = nan(size(thisMouse));
previousTrialOmitted(find(omittedrewards)+delta) = [thisMouse(omittedrewards).stimT];
previousTrialOmitted = previousTrialOmitted(1:numel(thisMouse));

% plot histograms
figure;
bins = 0:16;
binCenters = (bins(1:end-1) + bins(2:end)) / 2;

% by reward time on previous trial
subplot(2,1,1)
bar(binCenters, earlyLickHazard(thisMouse(...
    previousTrialReward<=8)), ...
    'barwidth', 1, 'facealpha', .5)
hold on
bar(binCenters, earlyLickHazard(thisMouse(...
    previousTrialReward>8)), ...
    'barwidth', 1, 'facealpha', .5)
title('By last reward')
legend('<8 s', '>8 s');
xlabel('Time from stim 1 onset (s)')
ylabel('Early lick hazard')
% by block
earlyBlock = strcmp({thisMouse.hazard}, 'early') | ...
    strcmp({thisMouse.hazard}, 'lateprobes');
lateBlock = strcmp({thisMouse.hazard}, 'late') | ...
    strcmp({thisMouse.hazard}, 'earlyprobes');

subplot(2,1,2);
bar(binCenters, earlyLickHazard(thisMouse(earlyBlock)), ...
    'barwidth', 1, 'facealpha', .5)
hold on
bar(binCenters, earlyLickHazard(thisMouse(lateBlock)), ...
    'barwidth', 1, 'facealpha', .5)
title('By block')
xlabel('Time from stim 1 onset (s)')
ylabel('Early lick hazard')
legend('Early block', 'Late block');

%%
lastRewardBins = [6:1:12];
hazardColormap = zeros(numel(lastRewardBins)-1,12);
for ind=1:numel(lastRewardBins)-1
    hazardColormap(ind,:) = ...
        earlyLickHazard(thisMouse(previousTrialReward > lastRewardBins(ind) & ...
        previousTrialReward <= lastRewardBins(ind+1)), 0:12);
end

figure;
subplot(2,1,1)
imagesc(flipud(hazardColormap))
set(gca,'YTick',1:numel(lastRewardBins),'YTickLabel',fliplr(lastRewardBins))
set(gca,'XTick',2:2:24,'XTickLabel',1:12)
xlabel('Time from stimulus onset (s)')
ylabel('Reward timing on previous trial (s)')
h = colorbar;
ylabel(h, 'early lick hazard')
caxis([0 .25])

hazardColormapOmission = zeros(numel(lastRewardBins)-1,12);
for ind=1:numel(lastRewardBins)-1
    hazardColormapOmission(ind,:) = ...
        earlyLickHazard(thisMouse(previousTrialOmitted > lastRewardBins(ind) & ...
        previousTrialOmitted <= lastRewardBins(ind+1)), 0:12);
end

subplot(2,1,2);
imagesc(flipud(hazardColormapOmission))
set(gca,'YTick',1:numel(lastRewardBins),'YTickLabel',fliplr(lastRewardBins))
set(gca,'XTick',2:2:24,'XTickLabel',1:12)
xlabel('Time from stimulus onset (s)')
ylabel('Early lick timing on previous trial (s)')
h = colorbar
ylabel(h, 'early lick hazard')
caxis([0 .25])

%%
figure; bar(0.5:1:11.5,hazardColormap(1,:), 'barwidth', 1, 'facealpha', .5)
hold on; bar(0.5:1:11.5,hazardColormapOmission(1,:), 'barwidth', 1, 'facealpha', .5)
xlabel('Time from stimulus onset (s)');
ylabel('Early lick hazard rate')
legend('Following reward at 6-7 s', 'Following omitted reward at 6-7 s', ...
    'location','northwest')