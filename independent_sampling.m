%%
bins = [ -.375 -.125 .125 .375 ];
clear lickRate
for ind = 1:size(X,2)
    for indBin = 1:numel(bins+1)
        if indBin==1
            lickRate(ind, indBin) = mean(y(X(:,ind)<= bins(1)));
        elseif indBin<=numel(bins)
            lickRate(ind, indBin) = mean(y(X(:,ind)>bins(indBin-1) & ...
                X(:,ind)<=bins(indBin)));
        else
            lickRate(ind, indBin) = mean(y(X(:,ind)>bins(indBin-1)));
        end
    end
    
    totalLickRate(ind) = mean(y(X(:,ind)~=0));
end
%%
figure;plot(lickRate - totalLickRate')

%%
clear indLickRate jointLickRate
thresh = 0;

for pos = 51:size(X,2)
for ind = 51:size(X,2)
    if ind==pos
        jointLickRate(ind,pos) = NaN;
    else
        jointLickRate(ind,pos) = mean(y(X(:,ind)>thresh & X(:,pos)>thresh & X(:,1)~=0));
    end
    
    if ind~=pos
    % number of licks when first position is fast
    x1 = sum(y(X(:,ind)>thresh & X(:,pos)<=thresh & X(:,1)~=0));
    % number of licks when second position is fast
    x2 = sum(y(X(:,pos)>thresh & X(:,ind)<=thresh & X(:,1)~=0));
    % number of trials when first position is fast
    n1 = numel((y(X(:,ind)>thresh & X(:,pos)<=thresh & X(:,1)~=0)));
    % number of trials when second position is fast
    n2 = numel((y(X(:,pos)>thresh & X(:,ind)<=thresh & X(:,1)~=0)));
    % number of trials and licks when neighter is fast
    xb = sum(y(X(:,ind)<=thresh & X(:,pos)<=thresh & X(:,1)~=0));
    nb = numel(y(X(:,ind)<=thresh & X(:,pos)<=thresh & X(:,1)~=0));
    
    x = [ xb x1 x2 ];
    n = [ nb n1 n2 ];
    
    fun = @(p) -tripleBinomialLikelihood(x, n, p);
    x0 = x./n;
    A = [ 1 -1 0; ... % first P (baseline) has to be smaller than other two
        1 0 -1 ];
    c = [ 0; 0 ];
    
    p = fmincon(fun, x0, A, c);
    b = p(1);
    p1 = p(2);
    p2 = p(3);
    
    p1 = (p1-b) / (1-b);
    p2 = (p2-b) / (1-b);

    indLickRate(ind,pos) = (1 - (1-p1)*(1-p2)*(1-b));
    else
        indLickRate(ind,pos) = NaN;
    end
end
end
%%
figure;
subplot(3,1,1);
imagesc(jointLickRate(51:100,51:100))
caxis([0 .0125])
colormap(parula)
xlabel('t_1');
ylabel('t_2');
set(gca,'xtick',[0.5:10:50.5],'XTickLabel',-2.5:.5:0, ...
    'ytick',[0.5:10:50.5],'YTickLabel',-2.5:.5:0, ...
    'yaxislocation','right')
title({'Lick probability given' 's(t_1)>0 and s(t_2)>0'},...
    'FontSize', 8)
axis equal
xlim([20.5 50.5])
ylim([20.5 50.5])
colorbar

subplot(3,1,2);
imagesc(indLickRate(51:100,51:100))
caxis([0 .0125])
colormap(parula)
xlabel('t_1');
ylabel('t_2');
set(gca,'xtick',[0.5:10:50.5],'XTickLabel',-2.5:.5:0, ...
    'ytick',[0.5:10:50.5],'YTickLabel',-2.5:.5:0, ...
    'yaxislocation','right')
title({'Lick probability prediction' 'based on independent sampling model'}, ...
    'FontSize', 8)
axis equal

xlim([20.5 50.5])
ylim([20.5 50.5])
colorbar

h = subplot(3,1,3);
diffMap = jointLickRate(51:100,51:100)-indLickRate(51:100,51:100);

diffMap(eye(size(diffMap))==1) = 0;
imagesc(diffMap)
caxis([-.2e-2 .2e-2])
colormap(h,cmap)
xlabel('t_1');
ylabel('t_2');
set(gca,'xtick',[0.5:10:50.5],'XTickLabel',-2.5:.5:0, ...
    'ytick',[0.5:10:50.5],'YTickLabel',-2.5:.5:0, ...
    'yaxislocation','right')
title({'Deviation from' 'independent sampling model'},...
    'FontSize', 8)
axis equal
xlim([20.5 50.5])
ylim([20.5 50.5])
colorbar
