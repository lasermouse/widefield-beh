X = {};
y = {};
lags = 100; 

for indT = 1:numel(ys)
    maxTime = min([numel(ys{indT}), rt(indT), change(indT)-1]);
    
    X{indT} = zeros(maxTime, lags);
    y{indT} = zeros(maxTime, 1);
    
    
    
    for indN = 1:maxTime
        X{indT}(indN,max([1,lags-indN+1]):lags) = ...
            ys{indT}(max([1,indN-lags+1]):indN);
    end
    
    if ~isnan(rt(indT)) && rt(indT)<=maxTime
        y{indT}(rt(indT)) = 1;
    end
end

%%
X = cell2mat(X(~noiseless & strcmp(hazard,'nonsplit'))');
y = cell2mat(y(~noiseless & strcmp(hazard,'nonsplit'))');

X = X / log(2);